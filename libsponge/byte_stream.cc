#include "byte_stream.hh"

// Dummy implementation of a flow-controlled in-memory byte stream.

// For Lab 0, please replace with a real implementation that passes the
// automated checks run by `make check_lab0`.

// You will need to add private members to the class declaration in `byte_stream.hh`

template <typename... Targs>
void DUMMY_CODE(Targs &&... /* unused */) {}

using namespace std;

ByteStream::ByteStream(const size_t capacity_)
    : capacity(capacity_), rwnd(capacity), bytes_r(0), bytes_w(0), s(), end(false) {}

size_t ByteStream::write(const string &data) {
    size_t len = data.size();
    if (rwnd >= len) {
        s = s + data;
        rwnd -= len;
        bytes_w += len;
        return len;
    } else {
        s = s + data.substr(0, rwnd);
        bytes_w += rwnd;
        int ans = rwnd;
        rwnd = 0;
        return ans;
    }
}

//! \param[in] len bytes will be copied from the output side of the buffer
string ByteStream::peek_output(const size_t len) const { return (s.substr(0, len)); }

//! \param[in] len bytes will be removed from the output side of the buffer
void ByteStream::pop_output(const size_t len) {
    // s = move(s.substr(len));
    s.erase(0,len);
    rwnd += len;
    bytes_r += len;
}

//! Read (i.e., copy and then pop) the next "len" bytes of the stream
//! \param[in] len bytes will be popped and returned
//! \returns a string
std::string ByteStream::read(const size_t len) {
    string ans = peek_output(len);
    pop_output(len);
    rwnd += len;
    return ans;
}

void ByteStream::end_input() { end = true; }

bool ByteStream::input_ended() const { return end; }

size_t ByteStream::buffer_size() const { return capacity - rwnd; }

bool ByteStream::buffer_empty() const { return rwnd == capacity; }

bool ByteStream::eof() const { return (end && rwnd == capacity); }

size_t ByteStream::bytes_written() const { return bytes_w; }

size_t ByteStream::bytes_read() const { return bytes_r; }

size_t ByteStream::remaining_capacity() const { return rwnd; }
